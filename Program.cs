using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;
using Aneka.Entity;
using Aneka.Threading;

namespace Aneka.Examples.Multiplier
{

    class Program
    {

        static void Main(string[] args)
        {
            try
            {
             Logger.Start();

                if (true)
                {
                    string confFile = @"\\Mac\Home\Desktop\ex_03_without_file\Multiplier - Copy\conf.xml";

                    // now we check for the configuration file.
                    if (args.Length == 1)
                    {
                        confFile = args[0];
                        if (File.Exists(confFile) == false)
                        {
                            Console.WriteLine("matrixMultiplier: [Error] configuration file not found. EXIT");
                            Console.WriteLine("usage: matrixMultiplier : conf-file");
                            return;
                        }
                    }

                    MMApplication app = new MMApplication();
                    app.ConfigPath = confFile;
                    try
                    {
                        app.Run();
                        Console.In.Read();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("matrixMultiplier: [Error] exception:");
                        Console.WriteLine("\tMessage: " + ex.Message);
                        Console.WriteLine("\tStacktrace: " + ex.StackTrace);
                        Console.WriteLine("EXIT");

                        string path = Util.GetProjectDir("Warhol", true);
                        path = System.IO.Path.Combine(path, IOUtil.GetErrorLogFileName());
                        IOUtil.DumpErrorReport(path, ex, "Aneka Thread Demo - Error log");

                        Console.WriteLine("Error log saved to: {0}", path);
                    }
                }
                else
                {
                    Program.ShowHelp();
                }
            }
            finally 
            { 
                Logger.Stop();
            }
        }

        private static void ShowHelp() 
        {
            Console.WriteLine("Matrix multiplication using Aneka threads");
            Console.WriteLine();
            Console.WriteLine("usage: multiplier conf_path");
            Console.WriteLine("where: ");
            Console.WriteLine(" conf_path : path to the configuration file for connecting to Aneka.");
            Console.WriteLine();

        }
    }
}

