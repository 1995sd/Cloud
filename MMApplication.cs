#region Namespaces

using System;
using System.Collections.Generic;	// Ilist<...> class.
using System.Text;				    // StringBuilder class.

using System.IO;				    // IOException (IO Errors management)
using System.Drawing;			    // Image and Bitmap classes.

using Aneka.Entity;			        // Aneka Common APIs for all models
using Aneka.Threading;			    // Aneka Thread Model
using System.Threading;			    // ThreadStart (AnekaThread initialization)

#endregion

namespace Aneka.Examples.Multiplier
{
    
    public class MMApplication
    {
        #region Properties

        protected string configPath;

        public string ConfigPath 
        {
            get { return this.configPath; }
            set { this.configPath = value; }
        }
        #endregion
    
        #region Implementation Fields

        protected Configuration configuration;

        protected AnekaApplication<AnekaThread, ThreadManager> application;

        protected IList<AnekaThread> running;
        public IList<Multiplier> multipliers;
        public IList<Multiplier> done;
        public int[] results;
        public string[] texts;
        public string text;
        public int DIM;




        public int[] nums;

        public int[][] x;

        public int[][] finalRes;



        public const int THREADS = 2;

        #endregion

        #region Public Methods

        public MMApplication() 
        { 
            
        }
  
        public void Run()
        {
            
            try
            {
                this.Init();
                this.StartExecution();

                this.WaitForCompletion();

                this.composeResults();
                
            }
            finally
            {
                if (this.application != null) 
                {
                    if (this.application.Finished == false) 
                    {
                        this.application.StopExecution();
                    }
                }
            }
        }

        private void composeResults()
        {
            int temp = 0;

            for (int i = 0; i < THREADS; i++)
            {
                finalRes[i] = this.multipliers[i].result;

                
                //this.multipliers[i].Print();
                //Console.Out.WriteLine("result " + i + " : " + results[i]);
                //temp += results[i];

                Console.WriteLine("dehshiri");

            }
            //Console.Out.WriteLine("Totoal Result: " + temp);



            int count = 0;
            for (int i = 0; i < THREADS; i++)
            {
                count += finalRes[i].Length;
            }

            int[] saeed = new int[count];

            var count2 = 0;
            for (int i = 0; i < THREADS; i++)
            {
                for (int j = 0; j < finalRes[i].Length; j++)
                {
                    saeed[count2] = finalRes[i][j];
                    count2 += 1;
                    
                }
            }

            Array.Sort(saeed);

            foreach (int i in saeed)
            {
                System.Console.WriteLine(i);
            }
            

            
        }
        #endregion

        #region Helper Methods

        protected void Init()
        {
            if (string.IsNullOrEmpty(this.configPath) == true)
            {
                this.configuration = Configuration.GetConfiguration();
            }
            else
            {
                this.configuration = Configuration.GetConfiguration(this.configPath);
            }

            this.application = new AnekaApplication<AnekaThread, ThreadManager>(this.configuration);

        }

        protected void StartExecution()
        {
            this.running = new List<AnekaThread>();
            this.multipliers = new List<Multiplier>();


            initMatrices(THREADS);

            createMultipliers(THREADS);


            /*
            foreach (Multiplier m in multipliers) {
                m.Apply();
                
            }
            */
            // creates an AnekaThread for each filter
            foreach (Multiplier m in multipliers)
            {

                AnekaThread thread = new AnekaThread(new ThreadStart(m.Apply), application);
                thread.Start();
                this.running.Add(thread);
            }

        }

        private void initMatrices(int thread)
        {

            nums = new int[] { 1, 47, 2, 12, 5, 18, 2, 48, 3, 14, 6, 20 };

            x = new int[thread][];

            int split = nums.Length / thread;

            int count = 0;

            for (int i = 0; i < thread; i++)
            {

                x[i] = new int[split];

                for (int j = 0; j < split; j++)
                {
                    count += 1;
             
                    var tmpInt = nums[(i * split) + j];

                    x[i][j] = tmpInt;
                }


            }


            finalRes = new int[thread][];
            for (int i = 0; i < thread; i++)
                finalRes[i] = new int[split];

        }

        private void createMultipliers(int threads)
        {
            Console.WriteLine("dehshiri");
            for (int i = 0; i < threads; i++)
            {
                Multiplier m = new Multiplier(threads, i, ref finalRes[i], ref x[i]);
                this.multipliers.Add(m);
            }
            Console.WriteLine("dehshiri");
        }


        protected void WaitForCompletion()
        {
            this.done = new List<Multiplier>();
            bool bSomeToGo = true;
            while (bSomeToGo == true)
            {
                foreach (AnekaThread thread in this.running)
                {
                    thread.Join();
                }

                for (int i = 0; i < this.running.Count; i++)
                {
                    AnekaThread thread = this.running[i];
                    if (thread.State == WorkUnitState.Completed)
                    {
                        this.running.RemoveAt(i);
                        i--;
                        Multiplier m = (Multiplier)thread.Target;
                        this.done.Add(m);
                    }
                    else
                    {
                        // it must be failed...
                        thread.Start();
                    }

                }

                bSomeToGo = this.running.Count > 0;
            }

        }

        #endregion
    }
}
