using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Aneka.Examples.Multiplier
{

    [Serializable]
    public class Multiplier 
    {
        public int[] result;
        public int[] numbers;
        public int thread_number;
        public int dim;

        public void Apply()
        {

            //foreach (int i in numbers)
            //{
             //   System.Console.WriteLine(i);
            //}

            Array.Sort(this.numbers);
            result = this.numbers;

            Console.Out.WriteLine("thread_number " + this.thread_number);

        }

        public Multiplier(int d, int number, ref int[] r, ref int[] num) 
        {

            this.dim = d;
            this.numbers = num;
            this.thread_number = number;
            this.result = r;
        }
        
        public void Print()
        {
            Console.Out.WriteLine("thread_number " + this.thread_number);
            //Console.Out.WriteLine("text " + this.dim.ToString());
        }
    }
}
